class Machine {
    constructor() {
        this.power = false;
    }

    turnOn() {
        this.power = true;
        console.log('Прибор включён');
    }

    turnOff() {
        this.power = false;
        console.log('Прибор выключен');
    }

    isTurnOn() {
        return this.turnOn();
    }
}

class HomeAppliance extends Machine {
    constructor() {
        super();
        this.net = false;
    }

    plugIn() {
        this.net = true;
        console.log('Подключён к сети');
    }

    plugOff() {
        this.net = false;
        console.log('Отключен от сети');
    }

    isPlugIn() {
        return this.plugIn();
    }
}

class WashingMachine extends HomeAppliance {
    constructor() {
        super();
    }

    run() {
        if (!this.isPlugIn()) {
            console.log('Стиральная машинка запущена');
            return this.plugIn();
        } else {
            console.log('Подключите машинку в сеть');
        }
    }
}

class LightSource extends HomeAppliance {
    constructor() {
        super();
    }

    setLevel(level = 0) {
        let illuminationLevel = 0;

        if (level < 1 || level > 100) {
            illuminationLevel = 0;
        } else {
            illuminationLevel = level;
        }

        console.log(`Освещенность равна ${illuminationLevel}`);
    }
}

class AutoVehicle extends Machine {
    constructor() {
        super();

        this.setPositionX = 0;
        this.setPositionY = 0;
    }

    setPosition(x, y) {
        this.setPositionX = x;
        this.setPositionY = y;
        console.log(`Координата по оси X - ${this.setPositionX} по оси Y - ${this.setPositionY}`);
    }
}

class Car extends AutoVehicle {
    constructor() {
        super();
        this.speed = 10;
    }

    setSpeed(speed) {
        this.speed = speed;

        console.log(`Скорость ${this.speed}`);
    }

    run(x, y) {
        if (!this.isTurnOn()) {
            let interval = setInterval(() => {
                if (this.setPositionX <= (x - this.speed)) {
                    this.setPosition(this.setPositionX + this.speed, this.setPositionY);
                } else {
                    this.setPositionX = x;
                }
                if (this.setPositionY <= (y - this.speed)) {
                    this.setPosition(this.setPositionX, this.setPositionY + this.speed);
                } else {
                    this.setPositionY = y;
                }
                if (this.setPositionX >= x && this.setPositionY >= y) {
                    clearInterval(interval);
                }
                console.log(this.setPositionX, this.setPositionY);
            }, 1000);
        }
    }
}


let bosch = new WashingMachine();
bosch.plugIn();
bosch.turnOn();
bosch.run();

let lightBulb = new LightSource();
lightBulb.plugIn();
lightBulb.setLevel(60);
lightBulb.turnOn();

let honda = new Car();
honda.setPosition(30, 40);
honda.turnOn();
honda.setSpeed(60);
honda.run(480, 340);